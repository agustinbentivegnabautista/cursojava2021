package modulo3;

import java.util.Scanner;

public class Ejercicio9 {

	public static void main(String[] args) {
		System.out.println("Primer participante, ingrese 0 para piedra, 1 para papel o 2 para tijeras");
		Scanner scan1 = new Scanner (System.in);
		int prim = scan1.nextInt();
		System.out.println("Segundo participante, ingrese 0 para piedra, 1 para papel o 2 para tijeras");
		Scanner scan2 = new Scanner (System.in);
		int seg = scan2.nextInt();
		if (prim >=0 && prim <=2 && seg >= 0 && seg <= 2) {
			if (prim == 0 && seg == 0)System.out.println("Empate");
			if (prim == 0 && seg == 1)System.out.println("Gana el segundo participante");
			if (prim == 0 && seg == 2)System.out.println("Gana el primer participante");
			if (prim == 1 && seg == 0)System.out.println("Gana el primer participante");
			if (prim == 1 && seg == 1)System.out.println("Empate");
			if (prim == 1 && seg == 2)System.out.println("Gana el segundo participante");
			if (prim == 2 && seg == 0)System.out.println("Gana el segundo participante");
			if (prim == 2 && seg == 1)System.out.println("Gana el primer participante");
			if (prim == 2 && seg == 2)System.out.println("Empate");
	}
		else System.out.println("Hubo un error");
}
}