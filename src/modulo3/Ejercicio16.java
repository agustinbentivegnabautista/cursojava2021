package modulo3;

import java.util.Scanner;

public class Ejercicio16 {

	public static void main(String[] args) {
		System.out.println("Ingrese el valor para ver su tabla de multiplicar --->");
		Scanner scan1 = new Scanner(System.in);
		int valor = scan1.nextInt();
		int mult = 1;
		System.out.println("tabla del "+valor);
		for (int i=0;i<10;i++) {
			mult = valor * i;
			System.out.println(valor+"x"+i+"   =   "+mult);
		}

	}

}
