package modulo3;

import java.util.Scanner;

public class Ejercicio15 {

	public static void main(String[] args) {
		System.out.println("Ingrese el tipo de auto --->");
		Scanner scan1 = new Scanner(System.in);
		char car = scan1.next().charAt(0);
		switch (car) {
		case 'a':
			System.out.println("El auto tiene 4 ruedas y un motor");
			break;
		case 'b':
			System.out.println("El auto tiene 4 ruedas, un motor, cierre centralizado y aire");
			break;
			
		case 'c':
			System.out.println("El auto tiene 4 ruedas, un motor, cierre centralizado, aire y airbag");
		
			break;
		default:
			System.out.println("Hubo un error");

	}
	}
}
