package modulo3;

import java.util.Scanner;

public class Ejercicio2 {

	public static void main(String[] args) {
		System.out.println("Ingrese un n�mero para saber si es par o impar --->");
		Scanner scan1 = new Scanner(System.in);
		int num = scan1.nextInt(); 
		int resto = num%2;
		if (resto != 0) {
			System.out.println("El n�mero "+ num +" es impar");
		}
		else {
			System.out.println("El n�mero "+ num + " es par");
		}
	}

}
